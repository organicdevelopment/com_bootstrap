
!function($){

    /**   TYPEAHEAD AJAX DATA-API
     *
     * Standard typeahead data options plus:
     *
     *  request-url:                        - the request url - this must be set to use this extension
     *  request-type:'post'                 - request type
     *  request-var: 'q'                    - request parameter
     *  request-text:'Loading...'           - loading text, blank for no loading text
     *  request-clear: 'icon-remove-sign'   - clear button class, blank for no button
     *  response-path: ''                   - optional path to the data items within the json response. Use dot syntax for path parts
     *  response-property: ''               - the property of the response object, may also use dot syntax for path
     *  response-target: ''                 - if set, the on select this element will have its value set
     *  response-target-property: ''        - the property in the data item to set in the target above
     *  submit-select: false                - auto submit the form on select
     *
     * ================== */

    $(function(){

        $('[data-provide="typeahead"]').each(function(){

            var $this = $(this);
            var typeahead_value = '';
            //Only process if request-url is set
            if($this.data('request-url'))
            {
                $this.data('source', function(query){
                    var typeahead = this;
                    typeahead.data = []

                    //Kill any existing request
                    if(typeahead.request) typeahead.request.abort()

                    var url = typeahead.options.requestUrl
                    var type = typeahead.options.requestType
                    var param = typeahead.options.requestVar

                    var data = {}
                    data[param] = query;

                    //Setup loader
                    if(typeahead.options.requestText) typeahead.process([typeahead.options.requestText]);

                    if(query) typeahead.clearButton.show()
                    else typeahead.clearButton.hide()

                    //Clear any previous timeout
                    if(typeahead.timeout) clearTimeout(typeahead.timeout)

                    //Run ajax request
                    typeahead.timeout = setTimeout(function(){

                        typeahead.request = $.ajax({
                            url: url,
                            dataType: 'json',
                            type: type,
                            data: data,
                            success: function(response){

                                //Hide icon
                                if(typeahead.options.requestIcon)
                                {
                                    $(typeahead.options.requestIcon).hide()
                                }

                                //Remove class
                                typeahead.$element.removeClass('requesting')

                                //Find items by path
                                if(typeahead.options.responsePath)
                                {
                                    var path = typeahead.options.responsePath.split('.')
                                    var tmp = response;
                                    $.each(path, function()
                                    {
                                        if(typeof(response[this]) == 'undefined') return;
                                        tmp = response[this]
                                    })
                                    response = tmp;
                                }

                                //Extract data from each item
                                if(typeahead.options.responseProperty)
                                {
                                    var items = []
                                    var path = typeahead.options.responseProperty.split('.')

                                    $.each(response, function(){

                                        var item = this;
                                        $.each(path, function(index)
                                        {
                                            if(typeof(item[this]) == 'undefined') return;

                                            //Set the data item in the typeahead data array
                                            if(index == path.length-1) typeahead.data[item[this]] = item;
                                            item = item[this]
                                        })
                                        items.push(item)
                                    })
                                    response = items;
                                }

                                //Run process to show the dropdown
                                typeahead.process(response);
                                typeahead.request = null;
                            }
                        });
                    }, 300)

                    //Trigger request event
                    $this.trigger('typeahead-request', [query]);

                    if(typeahead.options.requestIcon)
                    {
                        $(typeahead.options.requestIcon).show()
                    }

                    typeahead.$element.addClass('requesting')
                })

                //Sorting should be done by the server
                $this.data('sorter', function(items){
                    return items;
                })

                //Matching should be done by the server
                $this.data('matcher', function(item){
                    return true;
                })

                //Updater kills requests and returns the item text
                $this.data('updater', function(item){
                    if(this.request){
                        this.request.abort()
                        return '';
                    }

                    var data = this.data[item] ? this.data[item] : null;
                    $this.trigger('typeahead-select', [data]);

                    //Check if we have a target, if so, fire select event with data
                    if(typeahead.options.responseTarget && typeahead.options.responseTargetProperty)
                    {
                        var property = typeahead.options.responseTargetProperty.split('.')
                        $.each(property, function(index)
                        {
                            if(typeof(data[this]) == 'undefined') return;
                            data = data[this]
                        })

                        $(typeahead.options.responseTarget).val(data ? data[property] : '')
                    }

                    return item != this.options.requestText ? item : '';
                })

                //Initialize typeahead
                $this.typeahead($this.data())
                var typeahead = $this.data('typeahead')

                var options = $.extend({
                    'requestUrl': '',   //jquery converts request-url into requestUrl
                    'requestType':'post',
                    'requestVar':'q',
                    'requestText':'Loading...',
                    'requestClear': 'icon-remove-sign',
                    'responseProperty': '',
                    'responsePath': '',
                    'responseTarget': '',
                    'responseTargetProperty': '',
                    'submitSelect': false
                }, typeahead.options)
                typeahead.options = options;

                //Add clear button
                if(options.requestClear){
                    typeahead.wrapper = $('<span />', {'class':'typeahead-wrap','style':'position: relative; display: inline-block'})
                    typeahead.$element.css('margin-top','0')
                    typeahead.$element.css('margin-bottom','0')
                    typeahead.clearButton = options.requestClear.match(/^(\.|#)/) ? $(options.requestClear) : $('<span />', {'class':options.requestClear})
                    typeahead.clearButton.css('position','absolute')
                    typeahead.clearButton.css('top', '50%')
                    typeahead.clearButton.css('right', '0')
                    typeahead.clearButton.css('margin-top', typeahead.clearButton.outerHeight() / -2)
                    typeahead.clearButton.css('margin-right', typeahead.clearButton.outerWidth() / 2)

                    typeahead.clearButton.on('click', function(){
                        typeahead.clear()
                    })

                    $this.wrap(typeahead.wrapper).after(typeahead.clearButton.hide())
                }

                typeahead.clear = function(){
                    typeahead.$element.val('')
                    typeahead.clearButton.hide()
                    if(typeahead.request) typeahead.request.abort()
                }

                $this.on('blur', function() {
                    typeahead_value = $(this).val();
                    typeahead.clear()
                })

                $this.on('focus', function(){
                    typeahead_value = '';
                    if($(this).val()) typeahead.clearButton.show()
                })

                //this is to capture the return press when no options are returned or no options selected to force search
                $this.on('keypress', function(e) {
                    if (e.which === 13 && typeahead.shown === false) {
                        var v = $(typeahead.$menu).find('li').eq(0).attr('data-value');
                        var data = {};
                        switch (v)
                        {
                            case typeahead.requestText:
                            case undefined:
                            case 'undefined':
                                data[typeahead.options.requestVar] = $(this).val();
                                break;
                            default:
                                data[typeahead.options.requestVar] = v;
                                break;
                        }

                        if(typeahead.timeout) clearTimeout(typeahead.timeout)
                        e.preventDefault();
                        $this.trigger('typeahead-select', [data]);
                    }
                });


                //capture the click event on the submit button and fire the typeahead-select
                if (typeahead.$element.parent('span').siblings('button').length > 0) {
                    typeahead.$element.parent('span').siblings('button').click({em:$this}, function(e) {
                        if(typeahead.timeout) clearTimeout(typeahead.timeout)
                        e.preventDefault();
                        var data = {};
                        data[typeahead.options.requestVar] = typeahead_value;
                        $(e.data.em).val(typeahead_value).trigger('typeahead-select', [data]);
                    });
                }
                //Submit the form?
                if(typeahead.options.submitSelect)
                {
                    $this.on('typeahead-select', function(){

                        var select = this;
                        //Delay because the value gets set after this function is fired
                        setTimeout(function()
                        {
                            if($this.val()) {
                                select.form.submit()
                            }
                        }, 50)
                    })
                }
            }
        })
    })
}(window.jQuery)