<?php
/**
 * User: Oli Griffiths
 * Date: 06/09/2012
 * Time: 22:17
 */


class ComBootstrapTemplateHelperBootstrap extends KTemplateHelperDefault implements KServiceInstantiatable
{
    static protected $instance;
	protected $_scripts;
	protected $_stylesheets;
	protected $_jquery_version;
	protected $_min;
	protected $_baseURI;
	protected $_tmplURI;
	protected $_tmplPath;


    /**
     * Force creation of a singleton
     *
     * @param 	object 	An optional KConfig object with configuration options
     * @param 	object	A KServiceInterface object
     * @return KDatabaseTableDefault
     */
    public static function getInstance(KConfigInterface $config, KServiceInterface $container)
    {
        // Check if an instance with this identifier already exists or not
        if (!self::$instance)
        {
            //Create the singleton
            self::$instance  = new ComBootstrapTemplateHelperBootstrap($config);
            $container->set($config->service_identifier, self::$instance);
        }

        return self::$instance;
    }


	/**
	 * @param KConfig $config
	 */
	protected function _initialize(KConfig $config)
	{
		parent::_initialize($config);
		$config->append(array(
			'stylesheets' => array(
				'slim'          => false,
				'responsive'    => false,
				'bootstrap'     => false
			),
			'scripts' => array(
				'affix'         => false,
				'alert'         => false,
				'button'        => false,
				'carousel'      => false,
				'collapse'      => false,
				'dropdown'      => false,
				'modal'         => false,
				'tooltip'       => false,
				'popover'       => false,
				'scrollspy'     => false,
				'tab'           => false,
				'transition'    => false,
				'typeahead'     => false,
				'typeahead-ajax'=> false,
				'jquery'        => false,
				'jquery_noconflict' => false,
				'all'           => false
			),
			'jquery_version'   => '1.8.2',
			'min'        => true,
		));

		//Set the template
		$config->template = $this->getService('com://site/bootstrap.template.default');

		//Set the scripts and templates arrays
		$this->_scripts         = $config->scripts;
		$this->_stylesheets     = $config->stylesheets;
		$this->_jquery_version  = $config->jquery_version;
		$this->_min             = $config->min;

		//Assign template urls
		if(!KService::getAlias('application')){
			KService::setAlias('application', 'koowa:object.decorator');
			$app = $this->getService('application', array('object' => JFactory::getApplication()));
		}else{
			$app = $this->getService('application');
		}

		$template               = $app->getTemplate();
		$path_root              = JPATH_ROOT;
		$uri_root               = KRequest::root();
		$this->_baseURI         = 'media://com_bootstrap/';
		$this->_tmplURI         = $uri_root.'/templates/'.$template.'/html/com_bootstrap/assets/';
		$this->_tmplPath        = $path_root.'/templates/'.$template.'/html/com_bootstrap/assets/';
	}


	/**
	 * Gets the path to a file, check for override in component
	 * @param $file
	 * @return string
	 */
	protected function getPath($file)
	{
		if(file_exists($this->_tmplPath.$file)) $url = $this->_tmplURI.$file;
		else $url = $this->_baseURI.$file;

		return $url;
	}


	/**
	 * @param null $version
	 * @return ComBootstrapTemplateHelperBootstrap
	 */
	public function jquery($config = array())
	{
		$config = new KConfig($config);
		$config->append(array(
			'version' => null,
			'min' => null,
			'force' => false
		));

		if($config->version) $this->_jquery_version = $config->version;
		if($config->min !== null) $this->_min = $config->min;
		if($config->force) $this->_scripts->jquery = false;

		return $this->script('jquery');
	}


	/**
	 * Requests a script to be added to the render method
	 * @param array $config
	 * @return ComBootstrapTemplateHelperBootstrap
	 */
	public function script($config = array())
	{
		$config = (array) $config;

		//Collapse requires transitions
		if(in_array('collapse', $config)){
			$config[] = 'transition';
		}

		//Popover requires tooltip to come first
		if(in_array('popover', $config)){
			array_unshift($config,'tooltip');
		}

		$return = '';

		//Load jquery
		if(!$this->_scripts->jquery)
		{
			$min = $this->_min ? '.min' : '';
			$jquery = $this->getPath('js/jquery-'.$this->_jquery_version.$min.'.js');
			$return .= '<script src="'.$jquery.'" />'."\n";
			$this->_scripts->jquery = true;

			//Load noconflict
			if(!$this->_scripts->jquery_noconflict){
				$return .= '<script>;jQuery.noConflict();'."\n".'</script>'."\n";
				$this->_scripts->jquery_noconflict = true;
			}

            JFactory::getApplication()->set('jquery', true);
		}

		foreach($this->_scripts->toArray() AS $key => $value)
		{
			if(!$value && in_array($key, $config))
			{
				$name = $key.($this->_min ? '.min' : '');
				$path = $this->getPath('js/bootstrap-'.$name.'.js');
				$return .= '<script src="'.$path.'" />'."\n";
				$this->_scripts->$key = true;
			}
		}

		return $return;
	}


	/**
	 * Requests a stylesheet to be added to the render method
	 * @param array $config
	 * @return ComBootstrapTemplateHelperBootstrap
	 */
	public function style($config = array())
	{
		$config = (array) $config;

		if(empty($config)) $config[] = 'slim';

		$return = '';
		foreach($this->_stylesheets->toArray() AS $key => $value)
		{
			if(!$value && in_array($key, $config))
			{
				$path = $this->getPath('css/'.($key == 'all' ? 'bootstrap' : $key).'.css');
				$this->_stylesheets->$key = true;
				$return .= '<style src="'.$path.'" />'."\n";
			}
		}

		return $return;
	}


	/**
	 * Loads the relevant template and passes config data through
	 * @param string $method
	 * @param array $args
	 * @return mixed|string
	 */
	public function __call($method, $args)
	{
		if(!isset($this->_mixed_methods[$method]))
		{
			$script = $method;
			if(isset($this->_scripts->$script)){
				$return = '';
				if(!$this->_scripts->$script){
					$return = $this->script($script);
				}

				return $return;
			}else{
				throw new KTemplateHelperException('Bootstrap template helper called with invalid argument: '.$method.'. No method, template, script or style by that name');
			}
		}

		return parent::__call($method, $args);
	}
}